// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
//http://3.90.72.216:3010/ 
//http://192.168.1.88:3000/
//https://reportes-docker.nexusvirtual.net/
//URL_BASE: 'https://reportes-docker.nexusvirtual.net/satelital-facturacion-dev/',
export const environment = {
  production: false,
  //URL_BASE: 'http://192.168.1.88:3000/',
  URL_BASE: 'https://reportes-docker.nexusvirtual.net/integration-toro-dev/api/',
  // URL_BASE2: 'http://localhost:3000/api/',
  URL_BASE2: 'https://reportes-docker.nexusvirtual.net/satelital-efact/api/',
  URL_BASE3: 'https://reportes-docker.nexusvirtual.net/satelital-test/',
  URL_REPORTE: 'https://reportes-docker.nexusvirtual.net/satelital-facturacion-reporte/',
  URL_REPORTE_TEST: 'https://reportes-docker.nexusvirtual.net/reportes-satelital-test/',
  
  URL_SUNAT: 'https://reportes-docker.nexusvirtual.net/sunat/sunat/consulta/',

  LOGO_PREVISUALIZACION:'assets/img/satelital/logoHeader.png',
  MENSAJE_VALIDACION:{
    RUC:'El número de documento es menor a 11 dígitos.Ingrese un número de documento válido',
    BOLETA:'El número de documento es menor a 8 dígitos.Ingrese un número de documento válido',
    BOLETA_MAYOR:'El número de documento es mayor a 8 dígitos.Ingrese un número de documento válido',
    TIPO_CAMBIO_DESTINO:'Ingrese el tipo de cambio',
    NOMBRE_EMPRESA:'Ingrese la razon social'
  },
  MENSAJE_ERROR:{
    CONSULTA_SUNAT:'Ruc no encontrado.'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
