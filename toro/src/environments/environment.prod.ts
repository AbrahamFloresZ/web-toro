export const environment = {
  production: true,
  URL_BASE: 'https://reportes-docker.nexusvirtual.net/satelital-facturacion/',
  URL_SUNAT: 'https://reportes-docker.nexusvirtual.net/taxidirecto/sunat/consulta/'
};
