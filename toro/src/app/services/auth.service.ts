import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
const Swal = require('sweetalert2');
import { environment } from '../../environments/environment';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private _http: HttpClient
  ) { }

  login(email: string, password: string){
    if(email == "admin@gmail.com" && password =="123456"){
      this.router.navigate(['/pages']);
    }else{
      Swal.fire({
        title: "Credenciales Incorrectas",
        text: 'Las credenciales ingresadas no son las correctas',
        type: 'error',
        confirmButtonText: 'Ok'
      })
    }
  }
  
  requestLogin(data) {
    const url = environment.URL_BASE + "usuario/validate"; //error 404
    return this._http.post(url, data);
  }
}
