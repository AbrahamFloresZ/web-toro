import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class OtrosServiciosService {

    constructor(
        private _http: HttpClient
    ) { }

    listarSexo() {
        const url = environment.URL_BASE + "generic/lstgeneric"
        return this._http.get(url);
    }
    listarProcedencia(data: any) {
        const url = environment.URL_BASE + "generic/lstdptoxmunicipio";
        return this._http.post(url, data);
    }
    listarEstado() {
        const url = environment.URL_BASE + "generic/lstestado";
        return this._http.get(url);
    }
}