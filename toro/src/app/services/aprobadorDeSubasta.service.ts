import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
// import {  } from 'file-saver';

@Injectable({
    providedIn: 'root'
})
export class AprobadorDeSubastaService {
    ganado: any;
    constructor(
        private _http: HttpClient
    ) { }

    //listar
    filterGanado(data: any) {
        const url = environment.URL_BASE + "services/filterGanado";
        return this._http.post(url, data);
    }
    //obtener por id
    searchGanado(data: any) {
        const url = environment.URL_BASE + "services/filterGanado";
        return this._http.post(url, data);
    }
    UpdateGanado(data: any) {
        const url = environment.URL_BASE + "services/ganado";
        return this._http.post(url, data);
    }
}