import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute, CanLoad, Route } from '@angular/router';
import { HttpClient } from "@angular/common/http";
// import { Route } from '@angular/compiler/src/core';

@Injectable({
    providedIn: 'root'
  })

export class LoginGuard implements CanLoad {

    constructor(
        private router: Router,
        private _http: HttpClient
    ) { }

    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        return this.validarUsuario(state.url);
    }
    

    canLoad(route: Route){
        const url =  route.path;
        return this.validarUsuario(url);
    }

    validarUsuario(url :string):boolean {
        let user = localStorage.getItem("id");
        let rol = localStorage.getItem("rol");

        if(user){
            if(rol=='2'){
                if(url==='/pages/carga'){
                    this.router.navigate(['/pages/documento']);
                }
                return true;
            }else{
                return true;
            }
        }else{
            this.router.navigate(['/login']);
            return false;
        }
    }
}
