import { Component, OnInit } from '@angular/core';
import { AprobadorDeSubastaService } from '../../../services/aprobadorDeSubasta.service';

@Component({
  selector: 'app-listar-subastas',
  templateUrl: './listar-subastas.component.html',
  styleUrls: ['./listar-subastas.component.css']
})
export class ListarSubastasComponent implements OnInit {

  constructor(private service: AprobadorDeSubastaService) { }
  //boolean
  preload = false;
  //datos
  listaGanado = [];
  totalRegistros = 0;
  nRegistros = 20;
  nPaginas = 1;
  paginaActual = 1;
  ngOnInit() {
    this.getFullGanado();
  }
  getFullGanado() {
    const parametros = {
      "start": this.paginaActual,
      "draw": 1,
      "length": -1
    };
    this.preload = true;
    this.listaGanado = [];
    this.service.filterGanado(parametros).subscribe((data: any) => {
      this.listaGanado = data.List[0].data;
      this.totalRegistros = parseInt(data.List[0].total[0].TOTAL);
      let Paginas = (this.totalRegistros / this.nRegistros).toFixed();
      this.nPaginas = (parseInt(Paginas) <= 0) ? 1 : parseInt(Paginas);

      this.preload = false;
    }, (error) => {
      console.log(error);
      this.preload = false;
    });
  }
}
