import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprobadorDeSubastasComponent } from './aprobador-de-subastas.component';

describe('AprobadorDeSubastasComponent', () => {
  let component: AprobadorDeSubastasComponent;
  let fixture: ComponentFixture<AprobadorDeSubastasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprobadorDeSubastasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprobadorDeSubastasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
