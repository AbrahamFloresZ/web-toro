import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSubastasComponent } from './agregar-subastas.component';

describe('AgregarSubastasComponent', () => {
  let component: AgregarSubastasComponent;
  let fixture: ComponentFixture<AgregarSubastasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarSubastasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarSubastasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
