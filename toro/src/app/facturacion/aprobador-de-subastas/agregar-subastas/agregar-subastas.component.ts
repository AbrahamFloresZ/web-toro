import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { AprobadorDeSubastaService } from '../../../services/aprobadorDeSubasta.service';
import { OtrosServiciosService } from '../../../services/otrosServicios.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import * as moment from 'moment';

const Swal = require('sweetalert2')

@Component({
  selector: 'app-agregar-subastas',
  templateUrl: './agregar-subastas.component.html',
  styleUrls: ['./agregar-subastas.component.css']
})
export class AgregarSubastasComponent implements OnInit {

  constructor(
    private service: AprobadorDeSubastaService,
    private otrosServicios: OtrosServiciosService,
    // private fileSaverService: FileSaverService,
    private ruta: ActivatedRoute,
    private router: Router,
    private modalService: BsModalService
  ) { }
  //boolean
  preload: boolean = false;
  preloadSave: boolean = false;

  //configurar
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';
  dateInputFormat: string = 'DD/MM/YYYY';
  date = new Date();
  modalRef: BsModalRef;
  fechaActual: Date = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());

  //data
  fecha: any = "";
  listEstado = [];
  listSexoGrupo = [];
  listProcedencia = [];
  idGanado: number = 0;
  idGanadero: number = 0;
  ganadero: string = "";
  nroCedula: string = "";
  procedencia: string = "";
  grupoSexo: any = "";
  cantidad: any = "";
  pesoPromedio: any = "";
  pesoTotal: any = "";
  valorPorKilo: any = "";
  observaciones: string = "";
  urlVideo: string = "";
  urlDescargarVideo: string = "";
  urlImagen: string = "";
  comentario: string = "";
  estado: any = "";

  ngOnInit() {
    var miId = this.ruta.snapshot.paramMap.get('id');
    if (miId != null) {
      this.buscarGanado(parseInt(miId));
    }
    this.listarEstado();
    this.bsConfigDatePicker();
    this.listarSexoGrupo();
  }
  calcularPesoTotal() {
    var cant = this.cantidad != "" ? this.cantidad : 0;
    var pesoProm = this.pesoPromedio != "" ? this.pesoPromedio : 0;
    let miPesoTotal = (cant * pesoProm).toFixed(2);
    this.pesoTotal = (parseFloat(miPesoTotal));
  }
  verImagen(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'second', backdrop: 'static' })
  }
  guardarData() {
    let estado = this.validar();
    if (estado == true) {
      let data = {
        IdGanado: this.idGanado,
        IdGanadero: this.idGanadero,
        Sexo: this.grupoSexo,
        Procedencia: this.procedencia,
        Cantidad: parseInt(this.cantidad),
        Peso_total: parseFloat(this.pesoTotal),
        Peso_promedio: parseFloat(this.pesoPromedio),
        Observaciones: this.observaciones,
        precioBase: parseFloat(this.valorPorKilo),
        Fecha_ganado: moment(this.fecha).format("YYYY-MM-DDTHH:mm:ss.ms"),
        urlVideo: this.urlVideo,

        // valorxKilo: "",
        // NumeroDeLotes: "",
        // I002_EstadoSubasta: this.estado,
        // Ganadero: this.ganadero,
        //otros datos adicionales
        // nroCedula: this.nroCedula,
        // urlDescargarVideo: this.urlDescargarVideo,
        // urlImagen: this.urlImagen,
        // comentario: this.comentario,
        //
      };

      console.log("esto se envia", JSON.stringify(data));
      // return;
      this.preloadSave = true;
      this.service.UpdateGanado(data).subscribe((data: any) => {
        console.log("respuesta actualizar", data);
        let resultado = data.idResultado;
        if (resultado == 1) {
          Swal.fire({
            title: "Actualizado",
            text: "Ganado agregado",
            confirmButtonText: "ok"
          }).then((res) => {
            this.router.navigate(["/pages/aprobadorDeSubasta"])
          });
        } else if (resultado == 2) {
          Swal.fire({
            title: "Actualizado",
            text: "Ganado actualizado",
            confirmButtonText: "ok"
          }).then((res) => {
            this.router.navigate(["/pages/aprobadorDeSubasta"])
          });
        } else {
          Swal.fire({
            title: "Error",
            text: "No se pudo actualizar",
            confirmButtonText: "ok"
          })
        }
        this.preloadSave = false;
      }, (error) => {
        console.log("error aqui", error);
        this.preloadSave = false;
        Swal.fire({
          title: "Error",
          text: "Ocurrió un error inesperado, intenteló mas tarde",
          confirmButtonText: "ok"
        })
      });

    } else {
      Swal.fire({
        title: "Incompleto",
        html: "Ingresa los campos obligatorios<br>No te olvides de seleccionar un estado",
        confirmButtonText: "Ok"
      })
    }
  }
  descargarVideo() {

  }
  /*---------------------------------------------------*/
  private bsConfigDatePicker() {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme, dateInputFormat: this.dateInputFormat });
  }
  private listarEstado() {
    this.listEstado = [];
    this.otrosServicios.listarEstado().subscribe((data: any) => {
      let midata = data.List[0].Estado;
      this.listEstado = midata;
      // console.log("estad", {soloEstado:midata,Todo:data});
    });
  }
  private listarSexoGrupo() {
    this.otrosServicios.listarSexo().subscribe((data: any) => {
      let midata = data.List[0].Sexo;
      this.listSexoGrupo = midata;
    });
  }
  private buscarGanado(miIdGanado = -1) {
    if (miIdGanado > 0) {
      this.preload = true;
      const parametros = {
        start: 1,
        draw: 1,
        length: -1
      };
      this.limpiar();
      this.service.searchGanado(parametros).subscribe((midata: any) => {
        let dataGanadero = midata.List[0].data;

        if (dataGanadero.length > 0) {
          dataGanadero.forEach(element => {
            if (miIdGanado == element.IdGanado) {
              this.idGanado = miIdGanado;
              this.idGanadero = element.IdGanadero;
              this.ganadero = element.Nombres;
              this.nroCedula = element.numeroCedula != "" ? element.numeroCedula : "";
              this.procedencia = element.Procedencia != "" ? element.Procedencia : "";
              this.grupoSexo = element.Sexo != "" ? element.Sexo : "";
              this.cantidad = parseInt(element.Cantidad);
              this.pesoPromedio = (element.Peso_promedio).toFixed(2);
              this.pesoTotal = (element.Peso_total).toFixed(2);
              this.valorPorKilo = (element.precioBase).toFixed(2);
              this.observaciones = element.Observaciones;
              this.urlVideo = element.videoYoutube;
              this.urlDescargarVideo = element.urlArchivo;
              this.urlImagen = element.urlArchivoImagen != "" ? element.urlArchivoImagen : "https://www.lacubanaconecta.com/compra/img/p/es-default-home_default.jpg";
              this.comentario = element.Comentario != "" ? element.Comentario : "";
              // debugger
              this.fecha = element.Fecha_ganado != "" && element.Fecha_Ganado != "Invalid date" ? new Date(element.Fecha_ganado) : this.fechaActual;
              this.estado = element.I002_EstadoSubasta;

              let idDepartamento = element.IdDepartamento;
              this.listarProcedencia(idDepartamento);
              return;
            }
          });
        }
        this.preload = false;
      }, (error) => {
        console.log(error);
        Swal.fire({
          title: "error",
          text: "Ocurrió un error Intentalo más tarde",
          confirmButtonText: "ok"
        });
        this.preload = false;
      });
    }
  }
  private limpiar() {
    this.ganadero = ""
    this.nroCedula = ""
    this.procedencia = ""
    this.grupoSexo = ""
    this.cantidad = ""
    this.pesoPromedio = ""
    this.pesoTotal = ""
    this.valorPorKilo = ""
    this.observaciones = ""
    this.urlVideo = ""
    this.urlImagen = ""
    this.comentario = ""
    this.estado = ""
  }
  private validar() {
    let estado = false;
    if (this.ganadero != "" && this.nroCedula != "" && this.procedencia != "" && this.grupoSexo != "" && (this.cantidad != "" && this.cantidad > 0) && (this.pesoPromedio != "" && this.pesoPromedio > 0) && (this.valorPorKilo != "" && this.valorPorKilo > 0) && this.estado != "") {
      if (this.estado == 2 && this.urlVideo == "") {
        estado = false;
      } else {
        estado = true;
      }
    }
    return estado;
  }
  private listarProcedencia(miIdDepartamento = -1) {
    let item = {
      id_departamento: miIdDepartamento
    }
    this.listProcedencia = [];
    if (miIdDepartamento > 0) {
      this.otrosServicios.listarProcedencia(item).subscribe((data: any) => {
        let miData = data.List;
        this.listProcedencia = miData;
        console.log("procedencia", miData);
      });
    } else {
      this.listProcedencia.push({ id_municipio: this.procedencia, municipio: this.procedencia });
    }
  }
  
}