import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//compomentte
import { AprobadorDeSubastasComponent } from './aprobador-de-subastas.component';
import { ListarSubastasComponent } from './listar-subastas/listar-subastas.component';
import { AgregarSubastasComponent } from './agregar-subastas/agregar-subastas.component';

const routes: Routes = [
    {
        path: '', component: AprobadorDeSubastasComponent,
        children: [
            { path: '', component: ListarSubastasComponent },
            { path: "registrar", component: AgregarSubastasComponent },
            { path: "editar/:id", component: AgregarSubastasComponent },
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AprobadorDeSubastaRoutingModule { }