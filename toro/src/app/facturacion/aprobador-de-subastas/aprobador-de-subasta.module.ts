import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import {NgxPaginationModule} from 'ngx-pagination';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {ModalModule} from 'ngx-bootstrap/modal';
import {ListarSubastasComponent} from './listar-subastas/listar-subastas.component';
import {AgregarSubastasComponent} from './agregar-subastas/agregar-subastas.component';
import {AprobadorDeSubastasComponent} from './aprobador-de-subastas.component';
import {AprobadorDeSubastaRoutingModule} from './aprobador-de-subastas.routing.module';
// import {FileSaverModule} from 'ngx-filesaver';

const NgxBootstrapModule=[
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ];
  @NgModule({
    declarations: [
      AprobadorDeSubastasComponent,
      ListarSubastasComponent,
      AgregarSubastasComponent
    ],
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      AprobadorDeSubastaRoutingModule,
      NgxBootstrapModule,
      NgxPaginationModule,
      // FileSaverModule
    ]
  })
  
  export class AprobadorDeSubastaModule{

  }