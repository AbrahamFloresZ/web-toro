import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
//import {AprobadorDeSubastasComponent} from '../facturacion/aprobador-de-subastas/aprobador-de-subastas.component';
import {PagesComponent} from './pages.component'
const pagesRoutes: Routes = [
    {//parece que tambien se importaba un CommonModule?
        path:'',component:PagesComponent,
        //porque le pusiste default? para que no cayga  ¿como así?
        children:[
            { path: '', redirectTo: 'aprobadorDeSubasta',pathMatch: 'full'},
            { path: 'aprobadorDeSubasta', loadChildren: ()=>import('../facturacion/aprobador-de-subastas/aprobador-de-subasta.module').then(m=>m.AprobadorDeSubastaModule)  },//igual que ese jala
        ]
    }   
    
]
@NgModule({
    imports:[RouterModule.forChild(pagesRoutes)],
    exports:[RouterModule]

})
export class PagesModuleRouting{}
//export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);

//solo le cambio de nombre a component nada más