import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { PagesModuleRouting } from './pages.routes';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import {AprobadorDeSubastasComponent} from '../facturacion/aprobador-de-subastas/aprobador-de-subastas.component';
import {PagesComponent} from './pages.component'
import { NavbarComponent } from "../shared/navbar/navbar.component";
import { SidebarComponent } from "../shared/sidebar/sidebar.component";
defineLocale('es', esLocale);

@NgModule({
    declarations: [
        PagesComponent,
        NavbarComponent,
        SidebarComponent,
    ],
    exports: [        
        
    ],
    imports: [
        
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        PagesModuleRouting,
        BsDatepickerModule.forRoot(),
        PaginationModule.forRoot(),
        ButtonsModule.forRoot(),
        TimepickerModule.forRoot()
    ]
})

export class PagesModule {}