import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { PagesComponent } from './pages/pages.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { ModalModule } from 'ngx-bootstrap';
import { ListarSubastasComponent } from './facturacion/aprobador-de-subastas/listar-subastas/listar-subastas.component';
import { AgregarSubastasComponent } from './facturacion/aprobador-de-subastas/agregar-subastas/agregar-subastas.component';
import {PagesModule} from '../app/pages/pages.module'

@NgModule({
  declarations: [
    AppComponent,
    
    //PagesComponent,
    LoginComponent,
    //ListarSubastasComponent,
    //AgregarSubastasComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    //PagesModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
