import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

const Swal = require('sweetalert2')

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  login(forma: NgForm) {
    console.log(1);

    if (forma.invalid) {
      return;
    }

    const email = forma.value.email;
    const password = forma.value.password;
    let data={
      nombreCompleto:"admin",
      apellidoPaterno:"admin"
    };
    localStorage.setItem("user", JSON.stringify(data));
    this.authService.login(email,password);
    return;
    this.authService.requestLogin({ email: email, password: password }).subscribe(
      (result: any) => {

        if (result.resultado.resultado.code == 1) {
          Swal.fire({
            title: "Credenciales Incorrectas",
            text: 'Usuario incorrecto',
            type: 'error',
            confirmButtonText: 'Ok'
          })
        } else if (result.resultado.resultado.code == 2) {
          Swal.fire({
            title: "Credenciales Incorrectas",
            text: 'La Clave es incorrecta',
            type: 'error',
            confirmButtonText: 'Ok'
          })
        } else {
          localStorage.setItem("user", JSON.stringify(result.resultado.resultado));
          localStorage.setItem("id", result.resultado.resultado.idUsuarioFactura);
          localStorage.setItem("rol", result.resultado.resultado.idRol);

          if (result.resultado.resultado.idRol == '1') {
            this.router.navigate(['/pages']);
          } else {
            this.router.navigate(['/pages/aprobadorDeSubasta']);
          }
        }
      },
      (err: any) => {
        Swal.fire({
          title: "Credenciales Incorrectas",
          text: 'Las credenciales ingresadas no son las correctas',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      }
    )

  }
}
