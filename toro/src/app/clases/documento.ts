export class Documento {

    tipoDocumento: string;
    fechaEmision: Date;
    fechaVencimiento: Date;
    horaEmision: Date;
    correoReceptor: string;
    numeroDocumentoEmpresa: string;
    nombreEmpresa: string;
    tipoMoneda: string;
    tipoDocReceptor: string;
    serie: string;
    puntoVenta: string;
    detalleDocumento: ItemDetalle[];

    totalDescuento: number;
    direccionDestino:string;

    tipoCambioDestino: number;
    tipoPago:number;

    constructor(){
        this.numeroDocumentoEmpresa = "";
        this.tipoDocumento = "Factura";
        this.tipoMoneda = "PEN";
        this.tipoDocReceptor = "RUC";
        this.fechaEmision = new Date();
        this.fechaVencimiento=new Date();
        this.horaEmision = new Date();
        this.detalleDocumento = [];
        this.tipoCambioDestino = 3.33;
        this.tipoPago=1;
        this.nombreEmpresa="";
    }
}

export class ItemDetalle {

    cantidadItem: string;
    descripcion: string;
    tipoAfectacion: string;
    precioUnitario: number;

    constructor(){
        this.tipoAfectacion = "Exonerada";
    }
}