export class AprobacionSubasta {
    public ganadero: string;
    public nroCedula: any;
    public procedencia: string;
    public fecha: any;
    public grupo: string;
    public cantidad: number;
    public pesoPromedio: number;
    public pesoTotal: number;
    public valorporKg: number;
    public observaciones: string;
    public urlVideo: string;
    public imagenPortada: string;
    public comentario: string;
    public estado: any;

    constructor() {
        this.ganadero = "";
        this.nroCedula = "";
        this.procedencia = "";
        this.fecha = "";
        this.grupo = "";
        this.observaciones = "";
        this.urlVideo = "";
        this.imagenPortada = "";
        this.comentario = "";
        this.estado = "";
    }
}