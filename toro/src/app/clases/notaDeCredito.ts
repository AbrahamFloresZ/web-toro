export class NotaDeCredito{
    public tipoDeNota:any; //puede ser el id o el texto
    public numeroDeDocumentoReferente:string;
    public documento:string;
    public tipoDocumento:any; //puede ser el id o el texto
    public cliente:string;
    public RUC:string;
    public motivo:string;
    public comprobante:string;
    public empresa:string;
    public tipoMoneda:string;
    public tipoOperacion:string;
    public fechaEmision:string;
    public operacionGravada:any;
    public subTotal:any;
    public operacionExonerada:any;
    public IGVtotal:any;
    public descuentoTotales:any;
    public precioTotal:any;
    public montoDescuenti:number;
}